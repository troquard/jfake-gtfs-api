package eu.jfakegtfsapi;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.lang.InterruptedException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.ExecutionException;

/*
  getRootJSON(query, timeout) is the same as getRootJSON(query) but it
  will timeout after `timeout` seconds.
*/
class JSONUtils {

	private final static boolean VERBOSE = true;

	private static void log(String message) {
		if (VERBOSE) {
			System.out.println("_LOG_JSONUtils_\t\t\t" + message);
		}
	}

	public static JSONObject getRootJSON(String query) throws MalformedURLException, IOException {

		URL url = new URL(query);
		String content = IOUtils.toString(url, Charset.forName("UTF-8"));

		return new JSONObject(content);
	}

	private class ContentGetter implements Callable<String> {

		URL url;

		ContentGetter(URL url) {
			this.url = url;
		}

		@Override
		public String call() throws Exception {
			return IOUtils.toString(this.url, Charset.forName("UTF-8"));
		}

	}

	public static JSONObject getRootJSON(String query, int timeout)
			throws MalformedURLException, IOException, TimeoutException {

		URL url = new URL(query);

		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<String> future = executor.submit((new JSONUtils()).new ContentGetter(url));

		String content = null;
		try {
			log("Launched request with " + timeout + " seconds timeout.");
			content = future.get(timeout, TimeUnit.SECONDS);
			log("Answer received.");
		} catch (TimeoutException e) {
			future.cancel(true);
			log("Timeout!");
			// We raise it again.
			throw e;
		} catch (InterruptedException | ExecutionException e) {
			// We consume this one.
			e.printStackTrace();
		} finally {
			executor.shutdownNow();
		}

		return new JSONObject(content);
	}

}