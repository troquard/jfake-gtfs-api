package eu.jfakegtfsapi;

import java.text.ParseException;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;
import org.json.JSONArray;

import java.util.TimeZone;

/*
  This class works specifically with GTFS feeds accessed
  through API similar to:
  <a href="https://bitbucket.org/troquard/gtfs-fake-api">GTFS Fake API</a>.
*/
class FakeGTFSAPI {

	private final static boolean VERBOSE = true;

	private static void log(String message) {
		if (VERBOSE) {
			System.out.println("_LOG_FakeTransiAPI_\t\t" + message);
		}
	}

	public final static String JSON_RESULTS = "results";
	public final static String JSON_ROUTE = "route";
	public final static String JSON_HEADSIGN = "headsign";
	public final static String JSON_DEPARTURE_TIME = "departure_time";
	public final static String JSON_DEPARTURE_STOP_NAME = "departure_stop_name";
	public final static String JSON_ARRIVAL_TIME = "arrival_time";
	public final static String JSON_ARRIVAL_STOP_NAME = "arrival_stop_name";

	private final static String MIDNIGHT = "00:00:00";

	private final static int DEFAULT_TIMEOUT = 15;

	private String apiEndpoint;
	private TimeZone timeZone;
	private int timeout;

	public FakeGTFSAPI(String apiEndpoint, String timeZone) {
		this.apiEndpoint = apiEndpoint;
		this.timeZone = TimeZone.getTimeZone(timeZone);
		this.timeout = DEFAULT_TIMEOUT;
	}

	public FakeGTFSAPI(String apiEndpoint, String timeZone, int timeout) {
		this.apiEndpoint = apiEndpoint;
		this.timeZone = TimeZone.getTimeZone(timeZone);
		this.timeout = timeout;
	}

	private String getApiEndpoint() {
		return this.apiEndpoint;
	}

	public TimeZone getTimeZone() {
		return this.timeZone;
	}

	private int getTimeout() {
		return this.timeout;
	}

	public JSONArray getTrips(String origin, String destination, String date)
			throws ParseException, IOException, TimeoutException {

		return getTrips(origin, destination, date, MIDNIGHT);
	}

	public JSONArray getTrips(String origin, String destination, String date, String time)
			throws ParseException, IOException, TimeoutException {

		// We build the url string query...
		String query = getApiEndpoint() + origin + "/" + destination + "/" + date + "/" + time;
		log("URL QUERY : " + query);
		JSONObject rootJSON = null;
		// ... and make a JSONObject with the returned result.
		rootJSON = JSONUtils.getRootJSON(query, getTimeout());

		// We return the JSONArray of trips.
		return rootJSON.getJSONArray(JSON_RESULTS);
	}

}