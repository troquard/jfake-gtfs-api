package eu.jfakegtfsapi;

import org.json.JSONObject;
import org.json.JSONArray;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.lang.Exception;

public class App {

	private final static boolean VERBOSE = true;

	private static void log(String message) {
		if (VERBOSE) {
			System.out.println("_LOG_Main_\t\t\t" + message);
		}
	}

	// This function will print the trips on the standard output.
	private static void showTrips(FakeGTFSAPI fakeAPI, String origin, String destination, String date, String time)
			throws Exception {

		// We get the JSONArray of trips.
		JSONArray tripsJSON;
		tripsJSON = fakeAPI.getTrips(origin, destination, date, time);

		// We'll take the time zone into account.
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyyMMdd" + "HH:mm:ss");
		timeFormat.setTimeZone(fakeAPI.getTimeZone());

		// We'll reuse the JSONObject tripJSON for every trip.
		JSONObject tripJSON = null;
		// We'll reuse the Date time for every time in every trip.
		Date tripTime = null;

		// Finally, we pretty print every trip on the standard output.
		int numTrips = tripsJSON.length();
		log("Found " + numTrips + " trips.");
		for (int i = 0; i < numTrips; i++) {
			System.out.println("********** TRIP " + (i + 1));
			tripJSON = tripsJSON.getJSONObject(i);

			System.out.println("Route\t\t\t" + tripJSON.getString(FakeGTFSAPI.JSON_ROUTE));
			System.out.println("Headsign\t\t\t" + tripJSON.getString(FakeGTFSAPI.JSON_HEADSIGN));

			tripTime = timeFormat.parse(date + tripJSON.getString(FakeGTFSAPI.JSON_DEPARTURE_TIME));
			System.out.println("Departure time\t\t" + tripTime);
			System.out.println("Departure stop name\t" + tripJSON.getString(FakeGTFSAPI.JSON_DEPARTURE_STOP_NAME));

			tripTime = timeFormat.parse(date + tripJSON.getString(FakeGTFSAPI.JSON_ARRIVAL_TIME));
			System.out.println("Arrival time\t\t" + tripTime);
			System.out.println("Arrival stop name\t" + tripJSON.getString(FakeGTFSAPI.JSON_ARRIVAL_STOP_NAME));
		}
	}

	public static void main(String[] args) {

		final String fLIXBUS_ENDPOINT = "http://flixbus-fake-api.duckdns.org:8080/api/";
		final String eMT_MALAGA_ENDPOINT = "http://emt-malaga-fake-api.duckdns.org:80/api/";
		
		// Example of parameters : *Parque*del*Sur* *Plaza*de*la*Victoria* 20170811 12:05:00
		FakeGTFSAPI emtmalaga = new FakeGTFSAPI(eMT_MALAGA_ENDPOINT, "Europe/Paris", 60);
		// Example of parameters : *Paris* *Bordeaux* 20170803 09:00:00
		FakeGTFSAPI flixbus = new FakeGTFSAPI(fLIXBUS_ENDPOINT, "UTC", 30);
		
		try {
			System.out.format(
					"\n########## Trips EMT MALAGA " + "from %s to %s on the day %s at time %s ##########\n\n", args[0],
					args[1], args[2], args[3]);
			showTrips(emtmalaga, args[0], args[1], args[2], args[3]);
		} catch (Exception e) {
			// System.err.println(e);
			e.printStackTrace();
		}
	}
}
