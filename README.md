This is a Java app to query a GTFS "fake" APIs created with [GTFS-FAKE-API](https://bitbucket.org/troquard/gtfs-fake-api).

To setup, import as a Maven project.